## Local, Stage 1 ##
server {
    listen 80;
    root /app/public;
    server_name localhost downloader.local;
    access_log /var/log/nginx/downloader.ru.access.log main;
    error_log /var/log/nginx/downloader.ru.error.log warn;
    charset utf-8;

    server_name_in_redirect off;
    index index.php;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
    client_max_body_size 1024M;
    client_body_buffer_size 4M;
    error_page 404 /404;
    autoindex off;
    gzip on;
    gzip_vary on;
    gzip_min_length 1000;
    gzip_buffers 16 8k;
    gzip_proxied any;
    gzip_disable "msie6";
    gzip_comp_level 8;
    gzip_types *;

    #auth_basic "You shall not pass!";
    #auth_basic_user_file /etc/nginx/.htpasswd;

    # Redirect 301. Remove trailing slash from url
    rewrite ^(.+)/$ $1 permanent;

    # Redirect 301. Remove www. from host
    if ($host ~* ^www\.(.*)) {
      set $host_without_www $1;
      rewrite ^(.*) http://$host_without_www$1 permanent;
    }

    location / {
        try_files $uri @project;
    }

    location ~ \.php$ {
       try_files $uri @project;
       fastcgi_pass php-fpm;
       fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
       fastcgi_connect_timeout 3000;
       fastcgi_send_timeout 3000;
       fastcgi_read_timeout 3000;
       fastcgi_ignore_client_abort on;
       fastcgi_max_temp_file_size 0;
       send_timeout 3000;
       include fastcgi_params;
    }

    location @project {
        rewrite ^/(.*)$ /index.php?_url=/$1;
    }

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }
    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }
    location ~* ^.+\.(bmp|gif|jpg|jpeg|ico|png|swf|tiff|csv|xls|xlsx|css|js|svg|woff|woff2|ttf|eot)$ {
       access_log off;
       expires max;
       error_page 404 /404;
    }

    location ~ (/\.ht|\.svn|\.git) {
       deny all;
    }
}

## Php-fpm ##
upstream php-fpm {
    server 127.0.0.1:9001;
}