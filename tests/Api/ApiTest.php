<?php

namespace Tests\Api;
use App\Jobs\Downloader;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

/**
 *
 * Class ApiTest
 *
 * @package Tests\Api
 */
class ApiTest extends TestCase
{
    private $route;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->artisan('migrate');
        //$this->artisan('log:clear');

        $this->route = route('download.store');
        $this->withHeaders([
            'Accept' => 'application/json'
        ]);
    }

    /**
     * @return void
     */
    public function testWrongAddUrl(): void
    {
        $response = $this->post($this->route);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['url']);

        $response = $this->post($this->route, ['url' => '']);
        $response->assertStatus(422)
            ->assertJsonValidationErrors(['url']);

        // Not correct format
        $response = $this->post($this->route, ['url' => 'test']);
        $response->assertStatus(422)
            ->assertJsonValidationErrors(['url']);

        // Over 2048 symbols for GET URL (https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers)
        $longUrl = $this->getLongUrl();
        $response = $this->post($this->route, ['url' => $longUrl]);
        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function testSuccessAddUrl(): void
    {
        $datum = [
            ['url' => 'https://upload.wikimedia.org/wikipedia/ru/c/c9/%D0%A1%D0%B5%D1%80%D0%B3%D0%B5%D0%B9_%D0%94%D0%BE%D0%B2%D0%BB%D0%B0%D1%82%D0%BE%D0%B2.jpg'],
            ['url' => 'https://upload.wikimedia.org/wikipedia/commons/4/45/Yury_Dud_in_Nizhnekamsk.jpg'],
            ['url' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Aleksei_Balabanov_%282%29.jpg/1280px-Aleksei_Balabanov_%282%29.jpg']
        ];

        Queue::fake();

        foreach ($datum as $data) {
            $response = $this->post($this->route, $data);
            $response->assertStatus(201)
                ->assertJsonStructure(array_keys($data), $data);

            $id = $response->json('id');
            $this->assertDatabaseHas('download', array_merge($data, ['id' => $id]));

            Queue::assertPushed(Downloader::class, function ($job) use ($id) {
                /** @var Downloader $job */
                return $job->getModel()->id === $id;
            });

            //@todo Run job
        }
    }

    /**
     *
     */
    public function testListUrls(): void
    {
        $response = $this->get(route('download.create'));
        $response->assertStatus(200);
    }

    /**
     * @return string
     */
    private function getLongUrl(): string
    {
        return 'http://mail.ru/file.zip123123123123123412341234123411234123412341234234112341234123412341234123412341234123423412' . '341234123412123123123123123412341234123411234123412341234234112341234123412341234123412341234' . '123423412341234123412123123123123123412341234123411234123412341234234112341234123412341234123' . '412341234123423412341234123412123123123123123412341234123411234123412341234234112341234123412' . '341234123412341234123423412341234123412123123123123123412341234123411234123412341234234112341' . '234123412341234123412341234123423412341234123412123123123123123412341234123411234123412341234' . '234112341234123412341234123412341234123423412341234123412123123123123123412341234123411234123' . '412341234234112341234123412341234123412341234123423412341234123412123123123123123412341234123' . '411234123412341234234112341234123412341234123412341234123423412341234123412123123123123123412' . '341234123411234123412341234234112341234123412341234123412341234123423412341234123412123123123' . '123123412341234123411234123412341234234112341234123412341234123412341234123423412341234123412' . '123123123123123412341234123411234123412341234234112341234123412341234123412341234123423412341' . '234123412123123123123123412341234123411234123412341234234112341234123412341234123412341234123' . '423412341234123412123123123123123412341234123411234123412341234234112341234123412341234123412' . '341234123423412341234123412123123123123123412341234123411234123412341234234112341234123412341' . '234123412341234123423412341234123412123123123123123412341234123411234123412341234234112341234' . '123412341234123412341234123423412341234123412123123123123123412341234123411234123412341234234' . '112341234123412341234123412341234123423412341234123412123123123123123412341234123411234123412' . '341234234112341234123412341234123412341234123423412341234123412123123123123123412341234123411' . '234123412341234234112341234123412341234123412341234123423412341234123412123123123123123412341' . '234123411234123412341234234112341234123412341234123412341234123423412341234123412123123123123' . '123412341234123411234123412341234234112341234123412341234123412341234123423412341234123412';
    }
}
