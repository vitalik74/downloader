#!/bin/sh


###All###
cp ./database.example.env ./database.env
sudo docker-compose -f docker-compose.yml rm -s -f
sudo docker-compose -f docker-compose.yml build
sudo docker-compose -f docker-compose.yml up -d

#sudo docker-compose run --rm fpm bash -c "composer update && chown -R www-data:1000 /app/vendor /app/composer.lock"

#sudo docker-compose run --rm fpm bash -c "mkdir -p /app/docker/data/{db,app,nginx,php,sendmail,sphinx} /app/app/tmp /app/docker/data/php/profile && touch /app/app/tmp/db.log && touch /app/docker/data/app/application.log && chown -R www-data:1000 /app/docker/data/{app,nginx,php,sendmail,sphinx} /app/app/tmp /app/docker/data/php/profile"

