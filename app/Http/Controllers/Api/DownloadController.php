<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDownload;
use App\Services\DownloadService;

/**
 * Class FileStoreController
 *
 * @package App\Http\Controllers\Api
 */
class DownloadController extends Controller
{
    /**
     * @var DownloadService
     */
    private $service;

    public function __construct(DownloadService $service)
    {
        $this->service = $service;
    }

    /**
     * @param StoreDownload $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreDownload $request)
    {
        $validatedData = $request->validated();
        $model = $this->service->store($validatedData);

        return response()->json($model, 201);
    }

    /**
     * @param DownloadService $downloadService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(DownloadService $downloadService)
    {
        return response()->json($this->service->list(), 200);
    }
}
