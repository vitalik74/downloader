<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDownload;
use App\Services\DownloadService;

/**
 * Class IndexController
 *
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * @param StoreDownload $request
     * @param DownloadService $downloadService
     */
    public function store(StoreDownload $request, DownloadService $downloadService)
    {
        $validatedData = $request->validated();
        $downloadService->store($validatedData);

        return redirect('/');
    }
}
