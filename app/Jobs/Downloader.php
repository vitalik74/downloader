<?php

namespace App\Jobs;

use App\Models\Download;
use App\Services\DownloadService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class Downloader implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Download
     */
    private $model;
    /**
     * @var
     */
    private $service;

    /**
     * Create a new job instance.
     *
     * @param Download $download
     * @param DownloadService $downloadService
     */
    public function __construct(Download $download, DownloadService $downloadService)
    {
        $this->model = $download;
        $this->service = $downloadService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->service->job($this->model);
    }

    /**
     * @return Download
     */
    public function getModel(): Download
    {
        return $this->model;
    }
}
