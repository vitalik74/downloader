<?php

namespace App\Repositories;

use App\Models\Download;
use Illuminate\Support\Facades\DB;

/**
 * Class DownloadRepository
 *
 * @package App\Repositories
 */
class DownloadRepository
{
    /**
     * @param array $data
     *
     * @return Download
     */
    public function store(array $data): Download
    {
        return DB::transaction(function() use ($data) {
            return Download::create($data);
        });
    }

    /**
     * @param Download $model
     * @param int $status
     *
     * @return bool
     */
    public function updateStatus(Download $model, int $status): bool
    {
        return DB::transaction(function() use ($model, $status) {
            $model->status = $status;

            return $model->save();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function list()
    {
        return Download::all(['id', 'url', 'status', 'created_at', 'updated_at']);
    }
}
