<?php

namespace App\Services;

use App\Jobs\Downloader;
use App\Models\Download;
use App\Repositories\DownloadRepository;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

/**
 * Class DownloadService
 *
 * @package App\Services
 */
class DownloadService
{
    /**
     * @var DownloadRepository
     */
    private $repository;

    /**
     * DownloadService constructor.
     *
     * @param DownloadRepository $repository
     */
    public function __construct(DownloadRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     *
     * @return \App\Models\Download
     */
    public function store(array $data)
    {
        $model = $this->repository->store($data);

        // Add to Queue
        Downloader::dispatch($model, $this);

        return $model;
    }

    /**
     * @param Download $download
     */
    public function job(Download $download)
    {
        try {
            $this->repository->updateStatus($download, Download::DOWNLOADING);

            $client = new Client();
            $response = $client->request('GET', $download->url);

            $mimes = new \Mimey\MimeTypes;
            $extension = $mimes->getExtension($response->getHeaderLine('Content-Type'));

            $folder = substr(md5($download->url), 0, 10);
            $filename = substr(md5($folder), 0, 10);

            Storage::put($folder . '/' . $filename . (!empty($extension) ? '.' . $extension : ''), $response->getBody());

            $this->repository->updateStatus($download, Download::COMPLETE);
        } catch (\Exception $e) {
            $this->repository->updateStatus($download, Download::ERROR);
        }
    }

    /**
     * @return array
     */
    public function list()
    {
        return $this->repository->list();
    }
}
