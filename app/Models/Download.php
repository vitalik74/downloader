<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Download
 *
 * @property string $url
 * @property integer $status
 * @property string $file
 *
 * @package App\Models
 */
class Download extends Model
{
    public const PENDING = 0;
    public const DOWNLOADING = 10;
    public const COMPLETE = 20;
    public const ERROR = 30;

    /**
     * @inheritdoc
     */
    protected $fillable = ['url'];

    /**
     * @inheritdoc
     */
    protected $table = 'download';
}
